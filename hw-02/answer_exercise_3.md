Ejercicio 3.1
=============

Para el despliegue de los pods, se usará el siguiente objeto ReplicaSet:

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4-alpine
        resources:
          limits:
            cpu: 100m
            memory: 256Mi
          requests:
            cpu: 100m
            memory: 256Mi
```
Para desplegar el objeto ReplicaSet, se ejecuta el siguiente comando:
```
``kubectl apply -f [nombre_del_fichero_de_replicaset].yml``
```
Después de poner en marcha el ReplicaSet, es recomendable comprobar el estado de los pods con
```
``kubectl get pods -o wide``
```
Este comando nos dará también la IP de los pods, útil para comprobar el estado del servicio más adelante.

Después, es necesario desplegar el servicio, el cuál se muestra a continuación:

```
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  labels:
    app: nginx-server
spec:
  selector:
    app: nginx-server
  ports:
  - protocol: TCP
    port: 80
    name: http
    targetPort: 80
  type: NodePort
```
Se ha elegido el tipo de servicio NodePort porque es lo que permite exponer un servicio fuera del clúster.

El comando necesario es
```
kubectl -f apply [nombre_del_fichero_del_objeto_service].yml
```
Es importante comprobar que los selectores están nombrados correctamente, ya que de lo contrario, el servicio no detectará correctamente los pods.

Para comprobar el estado del servicio, se utiliza el comando
```
kubectl describe service nginx-server
```
En la fila "Endpoints" deberían estar las IP de los pods que nos ha proporcionado la ejecución del comando ``kubectl get pods -o wide`` mencionado anteriormente.

El comando ``minikube service nginx-server --url`` proporcionará la URL+puerto a la que hay que acceder para comprobar que el servicio ataca a los pods correctamente

Ejercicio 3.2
=============

Para el despliegue de los pods, se usará el siguiente objeto ReplicaSet:

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4-alpine
        resources:
          limits:
            cpu: 100m
            memory: 256Mi
          requests:
            cpu: 100m
            memory: 256Mi
```
Para desplegar el objeto ReplicaSet, se ejecuta el siguiente comando:
```
kubectl apply -f [nombre_del_fichero_de_replicaset].yml
```
Después de poner en marcha el ReplicaSet, es recomendable comprobar el estado de los pods con
```
kubectl get pods -o wide
```
Este comando nos dará también la IP de los pods, útil para comprobar el estado del servicio más adelante.

Después, es necesario desplegar el servicio, el cuál se muestra a continuación:

```
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  labels:
    app: nginx-server
spec:
  selector:
    app: nginx-server
  ports:
  - protocol: TCP
    port: 80
    name: http
```
Se ha eliminado el tipo NodePort de la definición del servicio. De acuerdo con la documentación de [Kubernetes](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types), si no se indica ningún tipo para el servicio, por defecto es de tipo ClusterIP, es decir, de uso interno para el clúster Kubernetes.

El comando necesario es
```
kubectl -f apply [nombre_del_fichero_del_objeto_service].yml
```
Es importante comprobar que los selectores están nombrados correctamente, ya que de lo contrario, el servicio no detectará correctamente los pods.

Para comprobar el estado del servicio, se utiliza el comando
```
kubectl describe service nginx-server
```
En la fila "Endpoints" deberían estar las IP de los pods que nos ha proporcionado la ejecución del comando ``kubectl get pods -o wide`` mencionado anteriormente.

A diferencia del apartado anterior, ahora no disponemos de una URL externa.

Para comprobar el funcionamiento del servicio, primero se obtiene la ClusterIP del servicio ejecutando ``kubectl get svc``.

Después, se accede a uno de los pods que el ReplicaSet puso en marcha mediante su nombre, con el comando ``kubectl exec --stdin --tty [NOMBRE_DE_UNO_DE_LOS_PODS] -- /bin/sh``

Una vez conectados al pod, mediante ``curl [CLUSTER_IP_DEL_SERVICIO]:80`` aparecerá el contenido html de la página de bienvenida de Nginx.

Ejercicio 3.3
=============

Para el despliegue de los pods, se usará el siguiente objeto ReplicaSet:

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4-alpine
        resources:
          limits:
            cpu: 100m
            memory: 256Mi
          requests:
            cpu: 100m
            memory: 256Mi
```
Para desplegar el objeto ReplicaSet, se ejecuta el siguiente comando:
```
kubectl apply -f [nombre_del_fichero_de_replicaset].yml
```
Después de poner en marcha el ReplicaSet, es recomendable comprobar el estado de los pods con
```
kubectl get pods -o wide
```
Este comando nos dará también la IP de los pods, útil para comprobar el estado del servicio más adelante.

Después, es necesario desplegar el servicio, el cuál se muestra a continuación:

```
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  labels:
    app: nginx-server
spec:
  selector:
    app: nginx-server
  ports:
  - protocol: TCP
    port: 80
    name: http
    targetPort: 80
    nodePort: 31245
  type: NodePort
```
Se ha elegido el tipo de servicio NodePort porque es lo que permite exponer un servicio fuera del clúster.

El comando necesario es
```
kubectl -f apply [nombre_del_fichero_del_objeto_service].yml
```
Es importante comprobar que los selectores están nombrados correctamente, ya que de lo contrario, el servicio no detectará correctamente los pods.

Para comprobar el estado del servicio, se utiliza el comando
```
kubectl describe service nginx-server
```
En la fila "Endpoints" deberían estar las IP de los pods que nos ha proporcionado la ejecución del comando ``kubectl get pods -o wide`` mencionado anteriormente.

Como se ve en los ajustes del servicio, se ha especificado un nodePort que hará falta añadir a los ajustes de red de la máquina virtual de minikube.

El comando ``minikube service nginx-server --url`` proporcionará la URL+puerto a la que hay que acceder para comprobar que el servicio ataca a los pods correctamente, aunque en este caso, esta información será la que hará falta introducir en los ajustes de red de la máquina virtual de minikube:

![minikube-network-settings](./images/iprule.png)

Finalmente, en el navegador de nuestro sistema operativo anfitrión, introducimos en un navegador la URL 127.0.0.1:31245 para comprobar que, efectivamente, sigue funcionando el servicio de Nginx.