Ejercicio 1
===========

Este es el fichero YAML de configuración:

```
apiVersion: v1
kind: Pod
metadata:
  name: nginx-server
spec:
  containers:
    - name: nginx
      image: nginx:1.19.4-alpine
      resources:
        limits:
          cpu: 100m
          memory: 256Mi
        requests:
          cpu: 100m
          memory: 256Mi
```

Para mostrar las 10 últimas líneas de la salida: ``kubectl logs --tail=10 nginx``\
Para obtener la IP interna del pod:\
![](./images/podip.png)\
Comando para entrar dentro del pod: ``kubectl exec --stdin --tty nginx-server -- /bin/sh``

Visualizar el contenido de Nginx: una vez habiendo entrado en el pod, usaría ``cat /usr/share/nginx/html/index.html``
* Como alternativa, se podría especificar un hostPort y un port para el contenedor
* Además de los puertos en la configuración, se podría añadir una regla nueva en el adaptador de red NAT de VirtualBox para usar los puertos del punto anterior para poder visualizar la página de bienvenida de Nginx en el navegador

Calidad de servicio del pod:

qosClass: Guaranteed (``kubectl get pod nginx-server --output=yaml``). La calidad de servicio es Guaranteed porque el pod tiene la CPU para limits y request iguales, así como la memoria asignada a limits y requests
