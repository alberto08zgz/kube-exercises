Ejercicio 2
===========

El siguiente fragmento YAML representa el objeto ReplicaSet solicitado

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4-alpine
        resources:
          limits:
            cpu: 100m
            memory: 256Mi
          requests:
            cpu: 100m
            memory: 256Mi
```

Para escalar el ReplicaSet a 10 réplicas:

```
``kubectl scale --replicas=10 rs/nginx-server``
```

Para tener una réplica en todos los nodos del clúster Kubernetes, [DaemonSet](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/) sería el objeto adecuado:

```
A DaemonSet ensures that all (or some) Nodes run a copy of a Pod. As nodes are added to the cluster, Pods are added to them. As nodes are removed from the cluster, those Pods are garbage collected. Deleting a DaemonSet will clean up the Pods it created.
```