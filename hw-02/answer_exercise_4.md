Ejercicio 4.1
=============

El objeto Deployment solicitado es el siguiente:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-server
  labels:
    app: nginx-server
spec:
  replicas: 3
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
        version: v1.0.0
    spec:
      containers:
      - name: nginx-server
        image: nginx:1.19.4-alpine
        resources:
          limits:
            cpu: 100m
            memory: 256Mi
          requests:
            cpu: 100m
            memory: 256Mi
        ports:
        - containerPort: 80
```
Para desplegar el objeto Deployment se utiliza el comando
```
kubectl apply -f [nombre_fichero_deployment].yml
```
Nótese que se ha especificado la estrategia de despliegue y una versión para este Deployment.

El objeto Service utilizado es el del apartado 1 del ejercicio 3:
```
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  labels:
    app: nginx-server
spec:
  selector:
    app: nginx-server
  ports:
  - protocol: TCP
    port: 80
    name: http
    targetPort: 80
    nodePort: 31245
  type: NodePort
```
Para poner en marcha el objeto Service se utiliza el comando
```
kubectl apply -f [nombre_fichero_service].yml
```

Ejercicio 4.2
=============

El objeto Deployment solicitado es el siguiente:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-server
  labels:
    app: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
        version: v2.0.0
    spec:
      containers:
      - name: nginx-server
        image: nginx:1.19.4-alpine
        resources:
          limits:
            cpu: 100m
            memory: 256Mi
          requests:
            cpu: 100m
            memory: 256Mi
        ports:
        - containerPort: 80
```

En este caso ha sido modificada la versión del objeto deployment para que kubectl detecte que ha habido un cambio en la configuración.

Para desplegar el objeto Deployment se utiliza el comando
```
kubectl apply -f [nombre_fichero_deployment].yml
```

El objeto Service empleado ha sido el mismo.

Ejercicio 4.3
=============

Para ver el histórico de versiones que ha habido para un objeto Deployment, se utiliza el comando ``kubectl rollout history deployment [nombre_del_deployment]``, que nos aporta un listado de las revisiones que ha habido y del motivo del cambio.

Para volver a la versión anterior se utilizaría el siguiente comando:
```
kubectl rollout undo deployment [nombre_del_deployment] --to-revision=1
```

