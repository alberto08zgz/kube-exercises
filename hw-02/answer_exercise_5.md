Ejercicio 5
===========

Deployment blue: este deployment sería el correspondido con los pods que tienen la versión 1 de la aplicación

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-server-blue
  labels:
    app: nginx-server-blue
spec:
  replicas: 3
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server-blue
  template:
    metadata:
      labels:
        app: nginx-server-blue
        version: v1.0.0
    spec:
      containers:
      - name: nginx-server-blue
        image: nginx:1.19.4-alpine
        resources:
          limits:
            cpu: 100m
            memory: 256Mi
          requests:
            cpu: 100m
            memory: 256Mi
        ports:
        - containerPort: 80
```
Service blue: lo llamaremos así para identificarlo como el servicio que, inicialmente, estará enlazado con los pods que corren la versión 1 de la aplicación

```
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  labels:
    app: nginx-server-blue
spec:
  selector:
    app: nginx-server-blue
  ports:
  - protocol: TCP
    port: 80
    name: http
    targetPort: 80
  type: NodePort
```
Deployment green: este deployment sería el correspondido con los pods que tienen la versión 2 de la aplicación

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-server-green
  labels:
    app: nginx-server-green
spec:
  replicas: 3
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server-green
  template:
    metadata:
      labels:
        app: nginx-server-green
        version: v2.0.0
    spec:
      containers:
      - name: nginx-server-green
        image: nginx:1.19.4-alpine
        resources:
          limits:
            cpu: 100m
            memory: 256Mi
          requests:
            cpu: 100m
            memory: 256Mi
        ports:
        - containerPort: 80
```
Service green: este servicio realmente tiene el mismo selector que el anterior, es decir, se llama igual que el anterior, pero apunta solo a los pods nuevos, es decir, a los que tendrán la versión 2 de la aplicación

```
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  labels:
    app: nginx-server-green
spec:
  selector:
    app: nginx-server-green
  ports:
  - protocol: TCP
    port: 80
    name: http
    targetPort: 80
  type: NodePort
```

El flujo de pasos a seguir sería el siguiente:
1. Se lanza el deployment blue: ``kubectl apply -f deploymentblue.yml``
2. Se lanza el service blue: ``kubectl apply -f serviceblue.yml``
3. Se comprueba que el servicio tiene asociados los pods correspondientes como endpoints: ``kubectl describe service nginx-server``
4. Se comprueba que el servicio está funcionando accediendo a la url proporcionada por este comando: ``minikube service nginx-server --url``
5. Se pone en marcha el deployment green (como el nombre de este deployment es diferente al deployment de la versión 1, el servicio existente no debería atacar a los pods nuevos de momento): ``kubectl apply -f deploymentgreen.yml``
6. Se comprueba que el servicio sigue teniendo exclusivamente a los pods v1 como endpoints:
    1. Primero se ejecuta ``kubectl get pods -o wide`` para ver las IP de los pods
    2. Después se comprueba con ``kubectl describe service nginx-server`` que sigue teniendo a los pods viejos como endpoints
7. Se lanza el servicio green (que en realidad se llama igual pero apunta al selector de los pods v2): ``kubectl apply -f servicegreen.yml``
6. Se comprueba que el servicio tiene exclusivamente a los pods v2 como endpoints:
    1. Primero se ejecuta ``kubectl get pods -o wide`` para ver las IP de los pods
    2. Después se comprueba con ``kubectl describe service nginx-server`` que solo tiene a los pods v2 como endpoints
