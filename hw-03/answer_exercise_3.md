Ejercicio 3
===========

El objeto HorizontalPodAutoscaler solicitado es el siguiente:

```
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: nginx-server-hpa
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: nginx-server
  minReplicas: 1
  maxReplicas: 10
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 50
```

Además del HPA anterior, son necesarios los siguientes objetos:
* Objeto Deployment:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-server
  labels:
    app: nginx-server
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
        version: v1.0.0
    spec:
      containers:
      - name: nginx-server
        image: nginx:1.19.4-alpine
        resources:
          limits:
            cpu: 100m
            memory: 256Mi
          requests:
            cpu: 100m
            memory: 256Mi
        ports:
        - containerPort: 80
```

* Objeto Service (este se usará para hacer pruebas cuando estén en marcha el Deployment y el HPA):

```
 app: nginx-server
spec:
  selector:
    app: nginx-server
  ports:
  - protocol: TCP
    port: 80
    name: http
    targetPort: 80
  type: NodePort
```

Los objetos descritos se aplican en el siguiente orden y con los comandos que se ven a continuación:
* 1º: se aplica el Deployment mediante ``kubectl apply -f [NOMBRE_FICHERO_DEPLOYMENT].yml``
* 2º: se aplica el Service mediante ``kubectl apply -f [NOMBRE_FICHERO_SERVICE].yml``
* 3º: se aplica el HorizontalPodAutoscaler mediante ``kubectl apply -f [NOMBRE_FICHERO_HPA].yml``

El estado inicial del HorizontalPodAutoscaler es el siguiente:

![hpa idle](./images/hpa_idle.png)

La primera captura ha sido realizada cuando el uso de la CPU no superaba el umbral:

![hpa one pod](./images/hpa_one_pod.png)

En la segunda captura se ve cómo el uso ya ha obligado al HPA a crear otra réplica:

![hpa two pods](./images/hpa_two_pods.png)

Estas 3 capturas fueron realizadas con un tiempo de sleep de 0.01s.\
Como tras esperar un tiempo que consideré suficiente el uso de la CPU no aumentaba, y con el objetivo de ver cómo desplegaba más réplicas, paré el comando de generación de peticiones para volverlo a lanzar un un sleep de 0.000000000000000000001s.

Después de realizar esta operación, pude ver cómo el consumo de CPU aumentaba de la siguiente forma:

![hpa three pods](./images/hpa_three_pods.png)