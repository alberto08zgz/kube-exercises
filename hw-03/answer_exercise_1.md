Ejercicio 1
===========

El objeto Ingress solicitado es el siguiente:

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-nginx-server
spec:
  tls:
  - hosts:
      - alberto.student.lasalle.com
    secretName: nginx-server-secret
  rules:
  - host: "alberto.student.lasalle.com"
    http:
      paths:
      - pathType: Prefix
        path: /
        backend:
          service:
            name: nginx-server
            port:
              number: 80
```

El objeto Secret solicitado es el siguiente:

```
apiVersion: v1
kind: Secret
metadata:
  name: nginx-server-secret
type: kubernetes.io/tls
data:
  tls.crt: MIIFLTCCAxWgAwIBAgIUS2KmOKJEXu5Vz3M/IDnJTqFG3CAwDQYJKoZIhvcNAQELBQAwJjEkMCIGA1UEAwwbYWxiZXJ0by5zdHVkZW50Lmxhc2FsbGUuY29tMB4XDTIwMTExODEzMDgwNloXDTIxMTExODEzMDgwNlowJjEkMCIGA1UEAwwbYWxiZXJ0by5zdHVkZW50Lmxhc2FsbGUuY29tMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAtG4wl4X3bUFVUkYi9xip6t8Q+NUZENmBofkB/c8DC6xnEOWzh5loMRUu1mv9Al3PxKu5+S7rWa1H1VziDetki0TZvhGGKQLJtihlon9eN7UUs0+0vXwM/NogPhC/BFr/h4uAF76n/d+UiOSLLH7Fzq8O3Rn12c0m57+TB9UGB9nR0nOUUsDW7hb6XIiI9rvgS3LHyyuWaBmRqP/dxPCdxGegINXRpgEx8yl8zVSGBABWxvTAIgRtZlLXYz8LhLxA7f37Bm65mrtyXWjfsiKlNH+nOG7akYszhPBfE9h1bIW5003r03UxuODmNGbE43Pkhw7+S/ayYAjy5ghFzLNM7HlN1I53IBBRjRW1VDILJCqn2sclR+RTCuoMPnxzfAyG6mO4s2sTCtjPf6W9VbqQZO0Q5KM14fKsks6hpt456yweu0SIdZLNWSU4xbGR1ueU48s0GEkiIJcbo7TSbQbtbNLhSQFoX6/E6+/ZZTtwttgWSffM8p23/LEbBTscg9qJFE/xROEyfx41RlR3iQmDVrcmXi3y7ytBNb6CvN1WKqtaMDYpOQnirjUATc2OcOR6eI7Lki1vrJXDAifqzB7LFwaXP/nGc/B7fZuXtwu0z1umvZINnqQrPhI87WlL1R12Nv4snOOBJudfaed2unNEivEKJ5iJ3INAc8X3gQiLku0CAwEAAaNTMFEwHQYDVR0OBBYEFDbuBVfe8B/ScE3r/MSYWlOqurwRMB8GA1UdIwQYMBaAFDbuBVfe8B/ScE3r/MSYWlOqurwRMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggIBAKlTSD1MhIEqeQGsGypD2glua5I4zng/rmeD+U2dr9uxWSnSJ3mCjNYlKNU6f08ZrrZCu9TX5asNwqi35NKbHDzz3ovO3m8Muv/fao8/fKTDD1S/Xt3MVhLuHF7eaXXk/rqUBHKPHl4hslbALCtDTQRrje81TTQy0VZrBAx4gFGXtWentDfD2L0UcXSbfsBdDXyG7Qlboj+hklo1c3fo3uonkVjcs0Tf5O+8VdRIH8cmQL4A/IvH4Q+T/igMSqyxoQou5RdsS6tVCGzp1Hp9XcJaNcuiL9XKl69AogEr7x8nroJsspfqrLSoF+nCk2pTArqLgMkxDSRkxVUha/pRRW6D0uiRNaHv+8vLiCfKdsv/avFcMkTneD2eSglnXy6HHMljYufGxpSJa5gXfLjGtNhISCNOi3uaMIhSiGzszw5nF2/qeFsxr2YIWtrItlz3w1zmIYMDxibI0xe0mUBj+oH5B4Cglc29U6VjLW2dkwxa4THljKK7cOg6uMUW0qOWzrZLauj1i8bWyI5eU4QTmFof3IQkd03NkpWipO917t2Ubf1L+u+ewD3+2oclARTWyNivqEdOhwaMwiuimJNTif20gF28oIICdyXQJ0Sks+woe7bTr0ucTQ8Du/R4juckQG6CEb5jKrz6bDe+3gCcGPdzZzc9maQirxVvVquDm++p
  tls.key: MIIJnDBOBgkqhkiG9w0BBQ0wQTApBgkqhkiG9w0BBQwwHAQIcQl/5iFj7F4CAggAMAwGCCqGSIb3DQIJBQAwFAYIKoZIhvcNAwcECA1q9mmupmpHBIIJSEdeOU34FOJ+QoEfDYcvZk1wvAtvKF953nbbf15gzCAuJAuujoNAEK7/J6YXXuOBhLs9qLUYjvaz+QfNcuhwaUmOOiW67LBEGut/Gksl81bGQyX+sTFwzi3N/LrhCrBul13FrPmjrRhCS21wq3z+i9JHe5YcxfOSsP8ELuKwDE312bX9SJrrbIctewaVnjt6jTMIs/T+kuTh/tDYVUxY2Z4yWqm3MJgNwEIb0652v9whRRWikJUoSMQnnaAQfMY+5GfqYm+ItS45gTzbC90CHNtiPjLURtBoE8/biOtsfSgud2g8EVLpIe+fVzfvtu9JvAY4Hy8/UlfAtEi3nI1yebHUAPx2GB+dr2S2fio91rVR8oVaKBr1Txp2ePW/yoEPCmDU39xAK04Hg1KmnIgFJmULJndwkHP9QIurS6JRwJM2SioccTWat+PzoIQLwwt4O8zPaLftT1d+euo3/B7NPBT6R+PGXMHM1DTaKgixRrMPtIkNfSZv4ngG2IjrLIj8Mhd9aHGMdWRGdRlm6f2cw6hI+sSa8mUJEYz2bLXaTKUyCTzjCI30L8oQLwhbWcuALR8RIQ4UuhBqL6pGavp1zo1od3fGESHnQxJ+AIBfF6DvV3aVec/zGbI9BxhXFIyJNKpgsZM/g0PMKNG7htds6SnKh+i8meSXhxrAY+BerollFYMT/SZGp/m4QkBg1mj9TExsJV9d5fq05cygyWlEoxTRmlQ0sPpyT0odWmMaqtYB20Rfg2+uXVZBdmhj7+cqWf1kH2ZpT8tgVmvgNWiIIZKf74JodRrs54mLVh3Hmzt3iG2GbkEG3DCj1HgD3uzVr08SPaNV84LaKAKprPOSZIwINuiJTrPyPCBiWwoNYxEFQigr0HgzYEDQCDrF9kTevMckykO7TBYczWYtlzwkEXmD95MuoqH/n0fKOT/O+/GrzPM0NFnUIHWyb4veHjC7G8y4IqAoOCbxRW1SYdOHRIHSWdMD2GEd2zfJ3ZnIoGMVcZJNlUxYzAwyNUb7Cd6THLzIS6v+bzrSRAU/uBWApz/v4+JIw+TLEOBOl8wl5gCxnbJ9fmwc1auj6bveI1DfkZFGFXlSnGflyz7ofJ1VtOpeIbYj57Y832DWNSPMNaYsUu9UDKEll+sm5EgfLSe7imHojtFZ5N2tjCqElSATYoN17DZa4F4MdDqVaYdz2ylK7ix7sIJ+RB8yzt0eW0DB7j6X5GU7fIN0TmADCLodWmt8eWEKnRiARmH97p69HZHxBj+GAm+znH9AHVjl5Bfiu6Aouk4CUEJqvYLTKMB3OsqJUx07UYje+PeqeGq8N32lE4YTKq4cBRhYU68fj8ECSwznSxXO1frZ1M22RQGp6kHSD6bpB3ttBCUJOH4kIL1i5+zMyKNzaMOFL6mtK89m3bhHD8TT880bAUwqW/jGXSXVOZUfJyRXA5aReIUs9DcJ7otoz72JkMXQqY9XrmAqg1C1GGAh/IG27oMw8/itL5zthm48PzTfZIZ9uQB/P0TyWBlA+UVdYJ6ivt0orDCB7DD6qM/bye9BtzOR9E64c4zfES2CDQMVrdlJSTG8pAH7yFy07zsWid0jyRYfwr1GpBNHrBTu/uRn8nMX3nxoIHsRfuVvSStQvfH9NHMvcKrbJAMtnA5nNR8uLrI8p0w0mU01fvDsJE8xKrdFkg9x2GQNwWXzzC062DRso3SbdZzbScZW+eLkqwrxk3t7oFsd8+amrshf+pQSAeV+TTtLlMUsrocPUyrjnd+h2+32LQCH8ZD9QJUoyYCs0FxlpVZoGz1bCaOGX1DztUESc2AwOOKBVkewkPYyk9dPeRtfYPC0KSQz8pIOvs+QjjjAfs87nMd7ebSzj8cdcggzb3ovXb0yXKQgImnrNj1/vPjJDRTv8p2w4IPb92CWgY0Ga/ROuMGNBFHUZkeJhoWLeYMzI/Q4e05apGpqgProbTi16FupHXIIRUqIuQARJvP8mJvGDaXp9szc1GtDT2YdXvl4Kj4ZOmyb68sp1R9PpjpXjYIe8iuuqlpate6oq74uoGq0Tv6Onj9ZbV5Io4Dgo2zuBqThZWTeJegCeYHy47ds67HtF3e65z9oD/H5arjBkQjlIqdMiCs6YaZq1uchbSo38HMG2isSDWl3MTJLmTi/OnfTE5oPFCb9C6+rc3b16lZnYCRdQLYCxtM/w8qWvFxISxkjIqgDCVFGZXB5uXU8AOz0XNLMRsr++a1gruYL8Sg2FCSdW9lHdUlb54F8Y0hsBIiG8obUJVFUyVyC5bVPLRIcqakPAZePjzFBuGpHVRALzI4i/VggVhbbjGIXO/FrFh0R1hx0BE6vlLtNtfkVbvitU9lUCH8vRYUk3sv0bZcGnYpZFWl7OyNJ2zuF1vqybVE8fslx3MDT/No99DPv3xxVcqone2Jao05n2zu/cmVeUZHKlmbYjXbnFxcvupDHar0lNjTqCiw9fvrSXJ2oGy/FHQL9HCpnn521scL8IS/sNVQYUbP1AQSNwHI1maqz1mIGaHlezVn3xXQzA0iV3e5AR+OJBptOSoSaPRO05hCl9ozI7bT5GbDaqxYaJL6SWcWEHNZb1EGFM0cb8leSsT8NltGzcXLmnAXZ4IyYCrG0OXhUzE1nOlyuSy5TW07SGeZEwGT9nPw79lRBUGzLS27PWXpH9CArAbeMgP8HQEtFbMq1EBp8DjmmIxfm1XkjklabuEmSwLUY32x6tyKAtp9l9S2mQ5UV27Wf0aGa/rbQbHAYcRLbaMApawcPvS6EPDfD0zlUzodxuP+42ml48uo5NlUvpKEqwiY18bOFPBcvKOpuG0vtgtdGlmUof8zABKJqs/N46aWnZIS+SbSNoTymEpxJgZIzkpDKMJVyMAVHqNFvqGA5bnmPBx03oVKMZsSYf0cb+D3Y9oslIEcxEru+GeL4/b/nbqMfeUb4HsTWYRkPszF0eO++lPQv/ce47PdSx4d22EmqJ6a82ShLms36xrc22zq5/hrjcFvp//e3ImKjjfiIYziHJbLhhygiPzYz01PuypHs5cBe59DQ+nWjnQR0gFJmO7z09qr33+ilnG7FTV0o6f+CbxP6dlLhkZw8SChW8DfmjEK7Ca/9ayyHBcomEeGfNY2YG0EJOKI/ha2eTeq+x7c9y6jLEViAYc/v95Rs0W9i/Q==
```

A continuación se presentan los objetos ReplicaSet y Service utilizados en este ejercicio:

* ReplicaSet

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4-alpine
        resources:
          limits:
            cpu: 20m
            memory: 128Mi
          requests:
            cpu: 20m
            memory: 128Mi
        ports:
        - containerPort: 80
```

* Service

```
apiVersion: v1
kind: Service
metadata:
  name: nginx-server
  labels:
    app: nginx-server
spec:
  selector:
    app: nginx-server
  ports:
  - protocol: TCP
    port: 80
    name: http
    targetPort: 80
  type: NodePort
```

El certificado usado en el objeto Secret se obtuvo mediante el comando ``openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -subj '/CN=alberto.student.lasalle.com'``

Importante: el uso de objetos Ingress debe habilitarse en minikube, preferiblemente antes de lanzar el objeto Ingress, con el comando ``minikube addons enable ingress``

Los objetos descritos se aplican en el siguiente orden y con los comandos que se ven a continuación:
* 1º: se aplica el ReplicaSet mediante ``kubectl apply -f [NOMBRE_FICHERO_REPLICASET].yml``
* 2º: se aplica el Service mediante ``kubectl apply -f [NOMBRE_FICHERO_SERVICE].yml``
* 3º: se aplica el Secret mediante ``kubectl apply -f [NOMBRE_FICHERO_SECRET].yml``
* 4º: se aplica el Ingress mediante ``kubectl apply -f [NOMBRE_FICHERO_INGRESS].yml``

Como el nombre de host que se ha configurado en el objeto Ingress no es un nombre publicado en ningún servidor DNS, se debe añadir al fichero hosts del sistema operativo que estemos usando.

Después de modificar el fichero hosts, el resultado se muestra de la siguiente forma:

![](./images/ingress-ssl.png)
