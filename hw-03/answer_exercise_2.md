Ejercicio 2
===========

El objeto StatefulSet solicitado es el siguiente:

```
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mongo
spec:
  selector:
    matchLabels:
      app: db
      name: mongodb
  serviceName: mongodb-svc
  replicas: 3
  template:
    metadata:
      labels:
        app: db
        name: mongodb
    spec:
      terminationGracePeriodSeconds: 10
      containers:
      - name: mongo
        image: mongo:4.4.1
        command:
          - mongod
        args:
          - --bind_ip=0.0.0.0
          - --replSet=rs0
          - --dbpath=/data/db1
        ports:
          - containerPort: 27017
        volumeMounts:
          - name: mongo-persistent-storage
            mountPath: /data/db1
  volumeClaimTemplates:
    - metadata:
        name: mongo-persistent-storage
      spec:
        accessModes: ["ReadWriteOnce"]
        resources:
          requests:
            storage: 1Gi
```
Nota: como ruta para la base de datos, se eligió /data/db1 porque (debido a unas pruebas fallidas realizadas previamente) la ruta /data/db no estaba disponible.

Pasos a seguir:

* 1º: Se pone en marcha el objeto StatefulSet con el comando ``kubectl apply -f [NOMBRE_FICHERO_STATEFULSET].yml``
* 2º: Para empezar a operar, nos conectamos al pod mongo-0 (elegimos este como maestro del cluster de mongo) con el comando ``kubectl exec --stdin --tty mongo-0 -- /bin/bash``
* 3º: Accedemos al terminal de mongo mediante ``mongo``
* 4º: Para asegurarnos de que la base de datos que usaremos con el nuevo usuario existe, ejecutamos el comando (exercise2 es el nombre elegido para la BD de este ejercicio) ``use exercise2``
* 5º: Para crear el usuario utilizamos la siguiente instrucción ```db.createUser({user: "alberto", pwd: "alberto", roles: [{ role: "readWrite", db: "exercise2" }]})``` (con esta instrucción le estamos dando permisos de lectura/escritura al usuario alberto para la base de datos exercise2)
* 6º: Para comprobar que el usuario se ha creado correctamente, utilizamos ``db.getUsers({showCredentials: true, filter: {}})``
* 7º: Ahora toca inicializar el cluster de mongo, de esta manera indicaremos qué pod es el maestro y cuáles los secundarios, que son los que recibirán la información escrita en el pod maestro. Para ello, se emplea el comando ``rs.initiate( {_id : "rs0", members: [{ _id: 0, host: "172.17.0.3:27017" }, { _id: 1, host: "172.17.0.4:27017" }, { _id: 2, host: "172.17.0.9:27017" }]})``
* 8º: Para comprobar que el cluster funciona correctamente primero hay que ejecutar (el siguiente comando nos da información de qué nodos son miembros del cluster de mongo) ``rs.status()``. Como demostración, se adjunta captura de pantalla que muestra los miembros del cluster de mongo durante el desarrollo de esta práctica. Cabe destacar que, la salida de este comando refleja que los 3 pods están sanos, además de qué nodo es el primario (en este caso el pod mongo-0,que es al que estamos conectados actualmente) y qué nodos son los secundarios
![pods members rs0](./images/rs_pods_mongo_members.png)
* 9º: Ahora hay que salir de la consola de mongo mediante ``exit``
* 10º: También hay que desconectarse del pod actual mediante ``exit``
* 11º: Para comprobar que la información del pod mongo-0 está replicada (el nuevo usuario en este caso), nos conectamos al pod (por ejemplo) mongo-1. Para ello, hay que ejecutar ``kubectl exec --stdin --tty mongo-1 -- /bin/bash`` 
* 12º: Ahora iniciamos sesión en la consola de mongo con el usuario alberto. Si se produce de forma exitosa, es que el usuario se replicó correctamente desde el pod mongo-0 a los demás pods. Utilizamos ``mongo --username alberto --pasword alberto --authenticationDatabase exercise2``
* 13º: Como comprobación extra, se puede ejecutar el comando ``db.runCommand({connectionStatus : 1})`` para ver que, en efecto, el usuario alberto está logueado

![user alberto connected](./images/user_alberto_connected.png).

Como anotación, cabe destacar que el en pod mongo-1, la terminal de mongo nos indica que este es un nodo de tipo SECONDARY.

* Diferencia entre realizar el ejercicio con ReplicaSet: con un objeto de tipo ReplicaSet, la configuración (en este caso la creación del usuario alberto) tendría que haber sido realizada en todos los pods, mientras que con el objeto StatefulSet bastó con configurar el usuario en un solo pod (mongo-0 en este caso) para que fuera posible acceder con ese usuario desde todos los pods.